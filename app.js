let importToDatabase = require('./tasks/importToDatabase');
let first = require('./tasks/task1');
let second = require('./tasks/task2');
let third = require('./tasks/task3');
let fourth = require('./tasks/task4');
let fifth = require('./tasks/task5');
let sixth = require('./tasks/task6');
let seventh = require('./tasks/task7');
let eigth = require('./tasks/task8');

const { JSDOM } = require( 'jsdom' );
const jsdom = new JSDOM();

// Set window and document from jsdom
const { window } = jsdom;
const { document } = window;
// Also set global window and document before requiring jQuery
global.window = window;
global.document = document;

const $ = global.jQuery = require( 'jquery' );


const functionsInit = function () {
    $(function () {
        // importToDatabase();
        // first();
        // second();
        // third();
        // fourth();
        // fifth();
        // sixth();
        // seventh();
        // eigth();
    });
};


let App = function () {
    this.initializeApp = function () {
        functionsInit();
    };
};

let application = new App();
application.initializeApp();
