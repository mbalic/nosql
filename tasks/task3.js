//Za svaku kategoričku vrijednost izračunati frekvencije pojavnosti po obilježjima varijabli i kreirati novi dokument koristeći nizove,dokument nazvati: 
// frekvencija_ {ime vašeg data seta} . Frekvencije računati koristeći $inc modifikator

var third = function () {
    const fs = require('fs');
    const express = require('express');
    const math = require('mathjs')
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });

    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    //shema diabetes fajlova
    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);

    var frekvencijaSchema = new mongoose.Schema({
        variable: String,
        variableValue: String,
        pojavnost: Number,
    });
    var FrekvencijaModel = mongoose.model('frekvencija_diabetesSet', frekvencijaSchema);

    DiabetesModel.find({}, function (err, doc) {
        if (err) throw err;

        var codes = [];
        doc.forEach((element) => {
            codes.push(element.code);
        });

        codes.sort();
        var current = null;
        var cnt = 0;
        for (var i = 0; i < codes.length; i++) {
            if (codes[i] != current) {
                if (cnt > 0) {
                    var statisticsJson = new FrekvencijaModel({ variable: "code", variableValue: current, pojavnost: cnt});
                    statisticsJson.save(function (err) {
                        if (err) return console.error(err);
                    });
                }
                current = codes[i];
                cnt = 1;
            } else {
                cnt++;
            }
        }
        if (cnt > 0) {
            var statisticsJson = new FrekvencijaModel({ variable: "code", variableValue: current, pojavnost: cnt});
            statisticsJson.save(function (err) {
                if (err) return console.error(err);
            });
        }
    });
}

module.exports = third;