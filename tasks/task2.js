//Za svaku kontinuiranu vrijednost izračunati srednju vrijednost, standardnu devijaciju i kreirati novi dokument oblika sa vrijednostima,
//dokument nazvati:  statistika_ {ime vašeg data seta}. U izračun se uzimaju samo nomissing  vrijednosti

var second = function () {
    const fs = require('fs');
    const express = require('express');
    const math = require('mathjs')
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });

    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    //shema diabetes fajlova
    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);
    
    var statisticSchema = new mongoose.Schema({
        variable: String,
        meanValue: Number,
        deviation: Number,
        numberOfElements: Number
    });

    DiabetesModel.find({}, function (err, doc) {
        if (err) throw err;

        var StatisticsModel = mongoose.model('statistika_diabetesSet', statisticSchema);

        getTextForValue(doc, math, StatisticsModel);
        getTextForDates(doc, math, StatisticsModel);
        //getTextForTimes(doc, math, StatisticsModel);
    });
}

function getTextForValue(doc, math, StatisticsModel) {
    var values = [];
    doc.forEach((element) => {
        if (Number.parseInt(element.value) && element.value !== "-1") {
            values.push(Number.parseInt(element.value));
        }
    });
    var meanValue = math.mean(values);
    var deviation = math.std(values);

    var statisticsJson = new StatisticsModel({ variable: "Value", meanValue: meanValue, deviation: deviation, numberOfElements: values.length });
    statisticsJson.save(function (err, test) {
        if (err) return console.error(err);
    });
}

function getTextForDates(doc, math, StatisticsModel) {
    var dates = [];
    doc.forEach((element) => {
        if (!dates.includes(element.date) && element.date !== "-1") {
            dates.push(element.date);
        }
    });
    dates.forEach((d) => {
        var valuesForDate = [];
        doc.forEach((el) => {
            if (el.date === d) {
                if (Number.parseInt(el.value) && el.value !== "-1") {
                    valuesForDate.push(Number.parseInt(el.value));
                }
            }
        });
        var meanValue = math.mean(valuesForDate);
        var deviation = math.std(valuesForDate);

        var statisticsJson = new StatisticsModel({ variable: "Date:" + d , meanValue: meanValue, deviation: deviation, numberOfElements: valuesForDate.length });
        statisticsJson.save(function (err, test) {
            if (err) return console.error(err);
        });
    });
}

function getTextForTimes(doc, math, StatisticsModel) {
    var times = [];
    doc.forEach((element) => {
        if (!times.includes(element.time) && element.time !== "-1") {
            times.push(element.time);
         }
    });

    times.forEach((t) => {
        var valuesForTime = [];
        doc.forEach((el) => {
            if (el.time === t) {
                if (Number.parseInt(el.value) && el.value !== "-1") {
                    valuesForTime.push(Number.parseInt(el.value));
                }
            }
        });
        if(valuesForTime.length > 0){
            var meanValue = math.mean(valuesForTime);
            var deviation = math.std(valuesForTime);

            var statisticsJson = new StatisticsModel({ variable: "Time:" + t , meanValue: meanValue, deviation: deviation, numberOfElements: valuesForTime.length });
            statisticsJson.save(function (err, test) {
                if (err) return console.error(err);
            });
        }
    });
}

module.exports = second;