//6.	Osnovni  dokument  kopirati u novi te embedati vrijednosti iz tablice 2 za svaku kontinuiranu  vrijednost kao niz :  emb2_ {ime vašeg data seta} 

var sixth = async function () {
    const express = require('express');
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });

    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);

    var statisticSchema = new mongoose.Schema({
        variable: String,
        meanValue: Number,
        deviation: Number,
        numberOfElements: Number
    });
    var StatisticsModel = mongoose.model('statistika_diabetesSet', statisticSchema);

    var diabetesEMB2Schema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String,
        valueEmb: [
            {
                meanValue: Number,
                deviation: Number,
                numberOfElements: Number
            }
        ],
        dateEmb: [
            {
                meanValue: Number,
                deviation: Number,
                numberOfElements: Number
            }
        ]
    });
    var DiabetesEMB2Model = mongoose.model("emb2_diabetesSet", diabetesEMB2Schema);

    const session = await db.startSession();
    let statValue = await StatisticsModel.findOne({ variable: "Value" }, null, { session, readPreference: 'secondary' });

    await DiabetesModel.find({}, function(error, doc){
        if (error) throw error;
        console.log("Setting emb values for Values")
        for (let i = 0; i < doc.length; i++) {
            var emb = new DiabetesEMB2Model({ date: doc[i].date, time: doc[i].time, code: doc[i].code, value: doc[i].value, valueEmb: { meanValue: statValue.meanValue, deviation: statValue.deviation, numberOfElements: statValue.numberOfElements } });
            emb.save(function (errr) {
                if (errr) return console.error(errr);
            });
        }
    });
    var statDate = [];
    await StatisticsModel.find({}, function(err, doc){
        if (err) throw err;

        console.log("Getting all statistic data for dates");
        for (let i = 0; i < doc.length; i++) {
            if(doc[i].variable.startsWith("Date:")){
                statDate.push(doc[i]);
            }
        }
    });

    for (let i = 0; i < statDate.length; i++) {
        var date = statDate[i].variable.split(":")[1];
        console.log("Setting statistic values for date " + date);
        await DiabetesEMB2Model.find({date:date}, async function(err, doc){
            if (err) throw err;

            for(let j = 0; j < doc.length; j++){
                await DiabetesEMB2Model.updateOne({_id:doc[j]._id}, {$set: {dateEmb: {meanValue:statDate[i].meanValue, deviation:statDate[i].deviation, numberOfElements:statDate[i].numberOfElements}}});
            }
        });
    }

    console.log("Done.");
}

module.exports = sixth;