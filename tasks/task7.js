//7.	Iz tablice emb2 izvući sve one srednje vrijednosti  iz  nizova čija je standardna devijacija 10% > srednje vrijednosti koristeći $set modifikator

var seventh = async function (){
    const express = require('express');
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });

    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    var diabetesEMB2Schema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String,
        valueEmb: [
            {
                meanValue: Number,
                deviation: Number,
                numberOfElements: Number
            }
        ],
        dateEmb: [
            {
                meanValue: Number,
                deviation: Number,
                numberOfElements: Number
            }
        ]
    });
    var DiabetesEMB2Model = mongoose.model("emb2_diabetesSet", diabetesEMB2Schema);
    var DiabetesDeviationTenPercGreater = mongoose.model("tenPercsGreaterDeviation_diabetesSet", diabetesEMB2Schema);

    await DiabetesEMB2Model.find({}, async function(err, doc){
        if (err) throw err;

        console.log("Started");
        for(let i = 0; i < doc.length; i++){
            if(doc[i].valueEmb[0] !== null && doc[i].valueEmb[0] !== undefined){
                var tenPrecentageOfMean = doc[i].valueEmb[0].meanValue * 0.1;
                if((doc[i].valueEmb[0].deviation - tenPrecentageOfMean) > doc[i].valueEmb[0].meanValue){
                    var json = new DiabetesDeviationTenPercGreater({
                        date: doc[i].date, 
                        time:doc[i].time, 
                        code:doc[i].code, 
                        value:doc[i].value, 
                        valueEmb:{
                            meanValue:doc[i].valueEmb[0].meanValue, 
                            deviation:doc[i].valueEmb[0].deviation, 
                            numberOfElements:doc[i].valueEmb[0].numberOfElements
                        }
                    });
                    if(doc[i].dateEmb[0] !== null && doc[i].dateEmb[0] !== undefined){
                        json.dateEmb.meanValue = doc[i].dateEmb[0].meanValue;
                        json.dateEmb.deviation = doc[i].dateEmb[0].deviation;
                        json.dateEmb.numberOfElements = doc[i].dateEmb[0].numberOfElements;
                    }
                    await json.save(function (err) {
                        if (err) return console.error(err);
                    });
                }
            }
            if(doc[i].dateEmb[0] !== null && doc[i].dateEmb[0] !== undefined){
                var tenPrecentageOfMean = doc[i].dateEmb[0].meanValue * 0.1;
                if((doc[i].dateEmb[0].deviation - tenPrecentageOfMean) > doc[i].dateEmb[0].meanValue){
                    var json = new DiabetesDeviationTenPercGreater({
                        date: doc[i].date, 
                        time:doc[i].time, 
                        code:doc[i].code, 
                        value:doc[i].value, 
                        valueEmb:{
                            meanValue:doc[i].valueEmb[0].meanValue, 
                            deviation:doc[i].valueEmb[0].deviation, 
                            numberOfElements:doc[i].valueEmb[0].numberOfElements
                        },
                        dateEmb:{
                            meanValue:doc[i].dateEmb[0].meanValue, 
                            deviation:doc[i].dateEmb[0].deviation, 
                            numberOfElements:doc[i].dateEmb[0].numberOfElements
                        }
                    });
                    await json.save(function (err) {
                        if (err) return console.error(err);
                    });
                }
            }
        }
    });
    console.log("Done");
}

module.exports = seventh;