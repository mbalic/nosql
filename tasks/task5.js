//5.	Osnovni  dokument  kopirati u novi te embedati vrijednosti iz tablice 3 za svaku kategoričku vrijednost, :  emb_ {ime vašeg data seta}

var fifth = async function () {
    const express = require('express');
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });

    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);

    var frekvencijaSchema = new mongoose.Schema({
        variable: String,
        variableValue: String,
        pojavnost: Number,
    });
    var FrekvencijaModel = mongoose.model('frekvencija_diabetesSet', frekvencijaSchema);

    var diabetesEMBSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String,
        pojavnostCoda: Number
    });
    var DiabetesEMBModel = mongoose.model("emb_diabetesSet", diabetesEMBSchema);

    var codes = [];
    await FrekvencijaModel.find({}, function(err, doc){
        if (err) throw err;

        console.log("Getting all statistic data for codes");
        for (let i = 0; i < doc.length; i++) {
            codes.push(doc[i]);
        }
    });

    for (let i = 0; i < codes.length; i++) {
        console.log("Setting statistic values for code " + codes[i].variableValue);
        await DiabetesModel.find({code:codes[i].variableValue}, async function(err, doc){
            if (err) throw err;

            for(let j = 0; j < doc.length; j++){
                var embJson = new DiabetesEMBModel({ date: doc[j].date, time:doc[j].time, code:doc[j].code, value:doc[j].value, pojavnostCoda:codes[i].pojavnost });
                await embJson.save(function (err) {
                    if (err) return console.error(err);
                });
            }
        });
    }

    console.log("Done");
}

module.exports = fifth;