//4.	Iz osnovnog  dokumenta kreirati dva nova dokumenta sa kontinuiranim vrijednostima u kojoj će u prvom dokumentu   
//biti sadržani svi elementi <= srednje vrijednosti , a u drugom dokumentu biti sadržani svi elementi >srednje vrijednosti ,
// dokument nazvati:  statistika1_ {ime vašeg data seta} i  statistika2_ {ime vašeg data seta} i 

var fourth = function () {
    const express = require('express');
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });

    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);

    var statisticSchema = new mongoose.Schema({
        variable: String,
        meanValue: Number,
        deviation: Number,
        numberOfElements: Number
    });
    var StatisticsModel = mongoose.model('statistika_diabetesSet', statisticSchema);
    var StatisticsModel1 = mongoose.model('statistika1_diabetesSet', diabetesSchema);
    var StatisticsModel2 = mongoose.model('statistika2_diabetesSet', diabetesSchema);

    StatisticsModel.find({}, function (err, doc) {
        if (err) throw err;

        var meanValue = 0;
        for (let i = 0; i < doc.length; i++) {
            if (doc[i].variable === "Value") {
                meanValue = doc[i].meanValue;
                break;
            }
        }
        DiabetesModel.find({}, function (err, doc) {
            if (err) throw err;

            doc.forEach((element) => {
                var pasredValue = 0;
                if (Number.parseInt(element.value)) {
                    pasredValue = Number.parseInt(element.value);
                };
                if (pasredValue <= meanValue) {
                    var statisticsJson = new StatisticsModel1({ date: element.date, time:element.time, code:element.code, value:element.value });
                    statisticsJson.save(function (err) {
                        if (err) return console.error(err);
                    });
                }
                else if (pasredValue > meanValue) {
                    var statisticsJson = new StatisticsModel2({ date: element.date, time:element.time, code:element.code, value:element.value });
                    statisticsJson.save(function (err) {
                        if (err) return console.error(err);
                    });
                }
            });
        });
    });
}

module.exports = fourth;