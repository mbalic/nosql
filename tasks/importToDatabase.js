var importToDatabase = function (DiabetesModel) {
    const fs = require('fs');
    const express = require('express');
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });


    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    //shema diabetes fajlova
    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);


    var obj;
    Number.prototype.pad = function (size) {
        var s = String(this);
        while (s.length < (size || 2)) { s = "0" + s; }
        return s;
    }
    for (let x = 1; x <= 70; x++) {
        var filename = 'files/data-' + x.pad();
        fs.readFile(filename, 'utf8', (err, data) => {
            if (err) throw err;
            obj = JSON.stringify(data);
            if(obj.charAt(0) === "\""){
                obj = obj.substr(1);
            };
            var rows = obj.split('\\n');
            for (let i = 0; i < rows.length; i++) {

                var columns = rows[i].split('\\t');

                if (columns.length === 4) {
                    var diabetesJson = new DiabetesModel({ date: columns[0], time: columns[1], code: columns[2], value: columns[3] });
                    if (!Number.parseInt(diabetesJson.value)) {
                        diabetesJson.value = -1;
                    };

                    diabetesJson.save(function (err, test) { //tu se spremi u mongo
                        if (err) return console.error(err);
                    });
                }
            }
        });
    }
}

module.exports = importToDatabase;