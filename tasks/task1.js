//Sve nedostajuće vrijednosti kontinuirane varijable zamijeniti sa -1, a kategoričke sa „empty“.

//Kategoričke varijable, koje se često nazivaju i kvalitativne, sadrže podatke kategorisane u grupe ili poređane po veličini.
    //Kategorički podaci razdvajaju ispitanike u jasno razgraničene grupe po određenoj karakteristici ili osobini.

//Kontinuirane varijable su one koje mogu imati bilo koju vrednost u okviru ranga koji predstavlja limit te varijable, kao što su: visina,težina,vreme i dr.

var first = function (){
    const fs = require('fs');
    const express = require('express');
    // initialize our express app
    const app = express();
    const mongoose = require('mongoose');
    let port = 1234;

    app.listen(port, () => {
        console.log('Server is up and running on port numner ' + port);
    });


    //https://mongoosejs.com/docs/index.html
    let dev_db_url = 'mongodb://localhost:27017/diabetesTest';
    let mongoDB = process.env.MONGODB_URI || dev_db_url;
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    //shema diabetes fajlova
    var diabetesSchema = new mongoose.Schema({
        date: String,
        time: String,
        code: String,
        value: String
    });
    var DiabetesModel = mongoose.model("diabetesSet", diabetesSchema);

    var dateQuery ={date:""};
    var dateValue = {$set: {date: '-1'} };
    DiabetesModel.updateMany(dateQuery, dateValue, function(err, res){
        if (err) throw err;
        console.log(res);
    })

    var timeQuery ={time:""};
    var timeValue = {$set: {time: '-1'} };
    DiabetesModel.updateMany(timeQuery, timeValue, function(err, res){
        if (err) throw err;
        console.log(res);
    })

    var codeQuery ={code:""};
    var codeValue = {$set: {code: "empty"} };
    DiabetesModel.updateMany(codeQuery, codeValue, function(err, res){
        if (err) throw err;
        console.log(res);
    })

    var valueQuery ={value:""};
    var valueValue = {$set: {value: '-1'} };
    DiabetesModel.updateMany(valueQuery, valueValue, function(err, res){
        if (err) throw err;
        console.log(res);
    })

    var valueQuery2 ={value:"3A"};
    var valueValue2 = {$set: {value: '-1'} };
    DiabetesModel.updateMany(valueQuery2, valueValue2, function(err, res){
        if (err) throw err;
        console.log(res);
    })
}

module.exports = first;